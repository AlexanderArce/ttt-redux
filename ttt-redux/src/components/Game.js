import React, {Component} from 'react'
import Board from './Board.js'
import { initGame } from '../actions/gameActions.js'

export default class Game extends Component {
  componentWillMount() {
    this.props.dispatch(initGame())
  }
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <Board {...this.props} />
        </div>
        <div className="game-info">
          <div>Turno de: {this.props.game.turno}</div>
          <ol>{}</ol>
        </div>
      </div>
    );
  }
}