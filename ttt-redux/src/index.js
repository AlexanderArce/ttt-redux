import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { Provider } from "react-redux"
import Game from './containers/Game'
import store from './store'


  // ========================================
  
const app = document.getElementById('root');
ReactDOM.render((
  <Provider store={store}>
    <Game/>
  </Provider>
), app);