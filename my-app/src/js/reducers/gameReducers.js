export default function reducer(state={
    history:[{
        squares: Array(9).fill(null),
    }],
    stepNumber:0,
    xIsNext:true,
}, action) {

    switch(action.type){
        case "HANDLE_CLICK":{
            const history = history.slice(0,stepNumber+1);
            const current = history[history.length - 1];
            const squares = current.squares.slice();
            if (calculateWinner(squares) || squares[i]) {
                return;
            }
            squares[i] = xIsNext ? 'X' : 'O';
            return {
                ...state,
                history: history.concat([{
                    squares: squares
                }]),
                stepNumber: history.length,
                xIsNext: !xIsNext,
            }
        }
        case "JUMP_TO":{
            return {
                ...state,
                stepNumber: step,
                xIsNext: (step%2)===0,
            }
        }
    }
}