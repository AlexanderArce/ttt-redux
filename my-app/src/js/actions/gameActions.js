export function handleClick(i){
    return {
        type: "HANDLE_CLICK",
        payload: {
            i,
        }
    }
}