import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Game from './Game';

  // ========================================
  
  const app = document.getElementById('root');
  ReactDOM.render(<Game />, app);